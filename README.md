**Wordpress project-starter manual**

Do not make any changes to this repository yourself!

---

## How to build the project

Please follow the instructions. In case of problems - inform your technical leader!

1. [Please install Composer](https://getcomposer.org/download/)
2. [Please install Node.js](https://nodejs.org/)
3. Change db configuration at file **config/db.php** - depends of your local db configuration.
4. To build project please correct **composer.json** file depending of your project configuration(add plugins, change version).
5. Open project folder in command line.
6. Write in the command line: **composer install** - install all packages, wordpress core, themes and other stuff.
7. Write in the command line: **composer update** - to check if all packages is installed.
8. Write in the command line: **composer dev-install** - if you work at dev env.
9. Write in the command line: **composer stg-install** - if you work at stage env.
10. Write in the command line: **composer prd-install** - if you work at prod env.
11. All plugins should be added only from command line. 
12. To add plugin after build project write in the command line: **composer require wpackagist-plugin/plugin-name : "*"**, where **plugin-name** you can get from [WordPress Packagist](https://wpackagist.org/) and ***** - version of plugin.
13. To remove plugin after build project write in the command line: **composer remove wpackagist-plugin/plugin-name**.
14. Write in the command line: **npm install** - install all packages for frontend part of work.
---

## How to work with project
1. All changes with theme you should do at folder **theme**!!!
2. Do not work with folder **build**!!!
3. Write in the command line: **gulp watch** - to watch project's files changes, compilling and minifing styles, scripts, images.
4. If you need to compille and minify styles, scripts, images just once - use: **gulp**.

---