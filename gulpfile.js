'use strict';

var themeName               = 'axiomo',
    gulp                    = require('gulp'),
    sass                    = require('gulp-sass')(require('sass')),
    cssmin                  = require('gulp-cssmin'),
    image                   = require('gulp-image'),
    imageminJpegRecompress  = require('imagemin-jpeg-recompress'),
    pngquant                = require('imagemin-pngquant'),
    babel                   = require('gulp-babel'),
    uglify                  = require('gulp-uglify-es').default,
    rename                  = require('gulp-rename'),
    concat                  = require('gulp-concat'),
    autoprefixer            = require('autoprefixer'),
    postcss                 = require('gulp-postcss'),
    pipeline                = require('readable-stream').pipeline,
    del                     = require('del'),
    stylesArray             = [
        themeName + '/assets/scss/main.scss'
    ],
    scriptsArray            = [
        themeName + '/assets/js/swiper-bundle.js',
        themeName + '/assets/js/main.js'
    ];

function styles(){
    return gulp.src(stylesArray)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest(themeName + '/assets/css'))        
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(themeName + '/assets/css/')); 
}

function scripts(){
    return gulp.src(scriptsArray)
        .pipe(babel({presets: ["@babel/preset-env"]}))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(themeName + '/assets/js/'));
}

function images(){
    return gulp.src(themeName + '/images/**/*.+(png|jpg|gif|svg)')
        .pipe(image({
            optipng: ['-i 1', '-strip all', '-fix', '-o7', '-force'],
            pngquant: ['--speed=1', '--force', 256],
            zopflipng: ['-y', '--lossy_8bit', '--lossy_transparent'],
            jpegRecompress: ['--strip', '--quality', 'medium', '--min', 40, '--max', 80],
            mozjpeg: ['-optimize', '-progressive'],
            gifsicle: ['--optimize'],
            svgo: ['--enable', 'cleanupIDs', '--disable', 'convertColors']
        }))
        .pipe(gulp.dest(themeName + '/assets/images'));
}

function watch(){
    gulp.watch(themeName + '/assets/scss/**/*.scss', styles);
    gulp.watch(scriptsArray, scripts);
    gulp.watch(themeName + '/images/**/*.+(png|jpg|gif|svg)', images);
}

var build = gulp.series(gulp.parallel(styles, scripts, images));

exports.styles = styles;
exports.scripts = scripts;
exports.watch = watch;
exports.build = build;

exports.default = build;