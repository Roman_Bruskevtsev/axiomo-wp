User-agent: *
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-json/
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: */embed
Disallow: */page/
Disallow: /cgi-bin
Disallow: /config
Disallow: *?s=
Disallow: *?replytocom
Disallow: /comments/feed/
Disallow: /xmlrpc.php
Disallow: /template.html
Disallow: /wp-content/themes/
Disallow: /wp-content/plugins
Disallow: */comments/
Disallow: /author/
Disallow: */feed
Disallow: /index
Disallow: *?p*
Disallow: /archive/	
Disallow: /archives/
Disallow: /category
Disallow: /category/uncategorized/
Disallow: /success-page/
Disallow: /html/
Allow: /wp-admin/admin-ajax.php


Sitemap: https://galera.agency/sitemap_index.xml
Host: https://galera.agency/