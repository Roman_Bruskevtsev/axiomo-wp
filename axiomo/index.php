<?php
/**
 *
 * @package WordPress
 * @subpackage Axiomo
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

    if( have_rows('content') ):
        while ( have_rows('content') ) : the_row();
            if( get_row_layout() == 'hero_section' ):
                get_template_part( 'template-parts/page/hero_section' );
            elseif ( get_row_layout() == 'page_banner_section' ) :
                get_template_part( 'template-parts/page/page_banner_section' );
            endif;
        endwhile;
    endif;

endwhile;

get_footer();