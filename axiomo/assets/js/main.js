'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
        });
    }

    windowLoad(){
        window.onload = function() {
            general.headerInit();
            general.heroSectionInit();
            general.companyIconInit();
            general.servicesIconInit();
        }
    }

    heroSectionInit(){
        let section = document.querySelector('.axm-hero__section'),
            scroll = document.querySelector('.axm-hero__scroll');
        if( !section ) return false;
        scroll.addEventListener('click', () => {
            window.scrollTo({
                top: section.getBoundingClientRect().height,
                behavior: "smooth"
            });
        });

        let backgroundSection = section.querySelector('.axm-hero__background'),
            windowHeight, topHeight, sectionHeight, sectionTop, scale;
        window.addEventListener('scroll', function() {
            topHeight = backgroundSection.getBoundingClientRect().top;
            sectionTop = - backgroundSection.getBoundingClientRect().top;
            windowHeight = window.innerHeight;
            sectionHeight = backgroundSection.getBoundingClientRect().height;
            scale = 1 + ( 0.5 * sectionTop ) / sectionHeight;
            console.log(scale);
            if( topHeight <= 0 ){
                
                backgroundSection.style['transform'] = `scale(${scale})`;
            } else {
                backgroundSection.style['transform'] = `scale(${1})`;
            }
        })
    }

    headerInit(){
        let header = document.querySelector('.axm-header'),
            btn = document.querySelector('.axm-show__menu'),
            menu = document.querySelector('.axm-main__menu'),
            items = menu.querySelectorAll('a'),
            mobileMenu = document.querySelector('.axm-mobile__menu'),
            mobileItems = mobileMenu.querySelectorAll('a');

        if( !btn ) return false;

        btn.addEventListener('click', () => {
            header.classList.toggle('show__menu');
            btn.classList.toggle('show__menu');
            mobileMenu.classList.toggle('show__menu');
            document.querySelector('body').classList.toggle('show__menu');
            document.querySelector('html').classList.toggle('show__menu');
        });

        items.forEach( (item) => {
            item.addEventListener('click', () => {
                items.forEach( (link) => {
                    link.classList.remove('active');
                });
                item.classList.add('active');
            });
        });

        mobileItems.forEach( (mobileItem) => {
            mobileItem.addEventListener('click', () => {
                mobileItems.forEach( (point) => {
                    point.classList.remove('active');
                });
                mobileItem.classList.add('active');

                header.classList.remove('show__menu');
                btn.classList.remove('show__menu');
                mobileMenu.classList.remove('show__menu');
                document.querySelector('body').classList.remove('show__menu');
                document.querySelector('html').classList.remove('show__menu');
            });
        });
    }

    companyIconInit(){
        let icon = document.getElementById('company-icon'),
            iconMobile = document.getElementById('company-icon-mobile');

        if( !icon ) return false;

        let iconHeight = icon.getBoundingClientRect().height,
            iconScroll = icon.getBoundingClientRect().top,
            iconHeightMobile = iconMobile.getBoundingClientRect().height,
            iconScrollMobile = iconMobile.getBoundingClientRect().top;

        if( iconScroll < iconHeight ) icon.classList.add('start__animation');
        if( iconScrollMobile < iconHeightMobile ) iconMobile.classList.add('start__animation');

        window.addEventListener('scroll', () => {
            iconScroll = icon.getBoundingClientRect().top;
            iconScrollMobile = iconMobile.getBoundingClientRect().top;
            if( iconScroll < iconHeight ) icon.classList.add('start__animation');
            if( iconScrollMobile < iconHeightMobile ) iconMobile.classList.add('start__animation');
        });
    }

    servicesIconInit(){
        let icon = document.getElementById('services-icon'),
            iconMobile = document.getElementById('services-icon-mobile');

        if( !icon ) return false;

        let iconHeight = icon.getBoundingClientRect().height,
            iconScroll = icon.getBoundingClientRect().top,
            iconHeightMobile = iconMobile.getBoundingClientRect().height,
            iconScrollMobile = iconMobile.getBoundingClientRect().top;

        if( iconScroll < iconHeight ) icon.classList.add('start__animation');
        if( iconScrollMobile < iconHeightMobile ) iconMobile.classList.add('start__animation');

        window.addEventListener('scroll', () => {
            iconScroll = icon.getBoundingClientRect().top;
            iconScrollMobile = iconMobile.getBoundingClientRect().top;
            if( iconScroll < iconHeight ) icon.classList.add('start__animation');
            if( iconScrollMobile < iconHeightMobile ) iconMobile.classList.add('start__animation');
        });
    }
}

let general = new GeneralClass();