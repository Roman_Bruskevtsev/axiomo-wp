<?php
/**
 *
 * @package WordPress
 * @subpackage Axiomo
 * @since 1.0
 * @version 1.0
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="color-scheme" content="light dark"> -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="axm-header<?php echo !is_front_page() ? ' dark' : ''; ?>" data-aos="fade-down" data-aos-duration="500" data-aos-delay="300">
        <div class="container">
            <div class="row">
                <div class="col-8 col-lg-6">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="axm-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-4 col-lg-6">
                    <?php if( has_nav_menu('main') ) { ?>
                    <div class="axm-show__menu d-block d-lg-none float-end">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="axm-main__menu d-none d-lg-block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'axm-main__nav d-none d-lg-block float-end'
                        ) ); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <?php if( has_nav_menu('main') ) { ?>
    <div class="axm-mobile__menu d-lg-none">
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'axm-mobile__nav'
        ) );
        if( get_field('address', 'option') ) { ?>
            <div class="axm-mobile__text address"><?php the_field('address', 'option'); ?></div>
        <?php }
        if( get_field('email', 'option') ) { ?>
            <div class="axm-mobile__text email">
                <p><?php _e('Email', 'axiomo'); ?></p>
                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a> 
            </div>
        <?php } ?>
    </div>
    <?php } ?>
    <main>