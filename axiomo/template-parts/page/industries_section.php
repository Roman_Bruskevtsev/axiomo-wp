<section class="axm-industries__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<div class="axm-content__block text-center" data-aos="fade-up" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
					<h6><?php the_sub_field('small_title'); ?></h6>
					<?php } 
					if( get_sub_field('title') ) { ?>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php }
					the_sub_field('text'); ?>
				</div>
				<?php 
				$icons = get_sub_field('icons');
				if( $icons ) { ?>
				<div class="axm-industries__icons">
					<div class="row">
					<?php foreach ( $icons as $item ) { ?>
						<div class="col-lg-4">
							<div class="axm-industries__icon text-center" data-aos="fade-up" data-aos-duration="500">
								<?php 
								if( $item['icon'] ) { ?>
								<div class="icon">
									<img src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['icon']['title']; ?>">
								</div>
								<?php }
								if( $item['title'] ) { ?><h6><?php echo $item['title']; ?></h6><?php } ?>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>