<?php 
$title = get_sub_field('title');
?>
<section class="axm-hero__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="axm-hero__background"<?php echo get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : ''; ?>></div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="axm-hero__wrapper">
					<div class="axm-hero__content text-center">
						<?php if( $title  ) { ?>
						<h1 data-aos="fade-up" data-aos-duration="500" data-aos-delay="300"><?php echo $title; ?></h1>
						<?php } ?>
						<div class="axm-hero__scroll">
							<span><?php _e('scroll down', 'axiomo'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>