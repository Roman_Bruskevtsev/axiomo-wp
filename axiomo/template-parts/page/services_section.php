<?php
$image = get_sub_field('image');
?>
<section class="axm-services__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="axm-content__block white" data-aos="fade-right" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
					<h6><?php the_sub_field('small_title'); ?></h6>
					<?php } 
					if( get_sub_field('title') ) { ?>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php } 
					if( $image ) { ?>
					<div class="image d-lg-none">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
					</div>
					<?php }
					the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if( $image ) { ?>
	<div class="image d-none d-lg-block">
		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
	</div>
	<?php } ?>
</section>