<?php
$vacancies = get_sub_field('vacancies');
$text = $vacancies ? get_sub_field('text') : get_sub_field('no_vacancies_text');
?>
<section class="axm-contacts__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : ''; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="axm-content__block" data-aos="fade-up" data-aos-duration="500">
					<?php
					if( get_sub_field('title') ) { ?>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
			<?php if( $text ) { ?>
			<div class="<?php echo $vacancies ? 'col-lg-4' : 'col-lg-9'; ?>">
				<div class="axm-content__block" data-aos="fade-up" data-aos-duration="500">
					<?php echo $text; ?>
				</div>
			</div>
			<?php }
			if( $vacancies ) { ?>
			<div class="col-lg-1"></div>
			<div class="col-lg-4">
				<div class="axm-content__block" data-aos="fade-up" data-aos-duration="500">
					<ul>
						<?php foreach( $vacancies as $vacancy ) { ?>
						<li>
							<a href="<?php echo get_permalink($vacancy->ID); ?>"><?php echo $vacancy->post_title; ?></a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>