<?php
/**
 *
 * @package WordPress
 * @subpackage Axiomo
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

    if( have_rows('content') ):
        while ( have_rows('content') ) : the_row();
            if( get_row_layout() == 'hero_section' ):
                get_template_part( 'template-parts/page/hero_section' );
            elseif ( get_row_layout() == 'about_us_section' ) :
                get_template_part( 'template-parts/page/about_us_section' );
            elseif ( get_row_layout() == 'services_section' ) :
                get_template_part( 'template-parts/page/services_section' );
            elseif ( get_row_layout() == 'industries_section' ) :
                get_template_part( 'template-parts/page/industries_section' );
            elseif ( get_row_layout() == 'contacts_section' ) :
                get_template_part( 'template-parts/page/contacts_section' );
            endif;
        endwhile;
    endif;

endwhile;

get_footer();