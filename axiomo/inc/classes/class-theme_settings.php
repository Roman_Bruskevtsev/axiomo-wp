<?php

class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.9';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_enqueue_scripts',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'wp_head', array( $this, 'google_tag_manager') );
		
		add_action( 'wp_body_open', array( $this, 'google_tag_manager_noscript') );
		add_action( 'init', array( $this, 'custom_post_type') );
		// add_action( 'init', array( $this, 'custom_taxonomy') );
        
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types' ), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );
		add_filter( 'script_loader_tag', array( $this, 'add_async_attribute' ), 10, 2 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'axiomo-google-montserrat-fonts', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap' , '', self::SCRIPTS_VERSION);
		wp_enqueue_style( 'axiomo-google-krona-fonts', 'https://fonts.googleapis.com/css2?family=Krona+One&display=swap' , '', self::SCRIPTS_VERSION);
		wp_enqueue_style( 'axiomo-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'axiomo-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'swiper-js', $this->scriptsDir.'/swiper-bundle.min.js', array( 'jquery', 'wp-polyfill', 'wp-polyfill-element-closest', 'wp-polyfill-node-contains' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'aos-js', $this->scriptsDir.'/aos.js', array( 'jquery', 'wp-polyfill', 'wp-polyfill-element-closest', 'wp-polyfill-node-contains' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'main-js', $this->scriptsDir.'/main.min.js', array( 'jquery', 'wp-polyfill', 'wp-polyfill-element-closest', 'wp-polyfill-node-contains' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	load_theme_textdomain( 'axiomo', get_template_directory().'/languages' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    // add_image_size( 'exrtise-thumbnail', 270, 180, true );

	    register_nav_menus( array(
	        'main'          	=> __( 'Main Menu', 'axiomo' ),
	        'footer-1'         	=> __( 'Footer Menu 1', 'axiomo' ),
	        'footer-2'         	=> __( 'Footer Menu 2', 'axiomo' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'axiomo'),
		        'menu_title'    => __('Theme Settings', 'axiomo'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}

		add_post_type_support( 'page', 'excerpt' );
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (company information)', 'axiomo' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'axiomo' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s about__widget">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

    public function __return_false() {
        return false;
    }

	public function add_async_attribute($tag, $handle){
	    if(!is_admin()){
	        if ('jquery-core' == $handle) {
	            return $tag;
	        }
	        return str_replace(' src', ' defer src', $tag);
	    } else {
	        return $tag;
	    }
	}

	public function custom_post_type(){
		$post_labels = array(
			'name'					=> __('Vacancies', 'axiomo'),
			'singular_name'			=> __('Vacancy', 'axiomo'),
			'add_new'				=> __('Add Vacancy', 'axiomo'),
			'add_new_item'			=> __('Add New Vacancy', 'axiomo'),
			'edit_item'				=> __('Edit Vacancy', 'axiomo'),
			'new_item'				=> __('New Vacancy', 'axiomo'),
			'view_item'				=> __('View Vacancy', 'axiomo')
		);

		$post_args = array(
			'label'               	=> __('Vacancies', 'axiomo'),
			'description'        	=> __('Vacancy information page', 'axiomo'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title' ),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'vacancies'
			),
			'menu_icon'           	=> 'dashicons-buddicons-buddypress-logo'
		);
		register_post_type( 'vacancy', $post_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => _x('Brands', 'axiomo'),
			'singular_name'               => _x('Brands', 'axiomo'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'brands',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);

		register_taxonomy( 'brands', 'product', $taxonomy_args );
	}
}

$theme_settings = new ThemeSettingsClass();