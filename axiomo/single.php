<?php
/**
 *
 * @package WordPress
 * @subpackage Axiomo
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if ( have_posts() ) { 

	while ( have_posts() ) { the_post(); 
        $description = get_field('description');
        $column_1 = get_field('responsibilities');
        $column_2 = get_field('requirements');
        ?>
    <section class="axm-single__content">
        <div class="axm-single__header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="axm-single__title" data-aos="fade-up" data-aos-duration="500">
                            <h1 class="h2"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <?php if( $description ) { ?>
                    <div class="col-lg-8">
                        <div class="axm-single__description" data-aos="fade-up" data-aos-duration="500">
                            <?php echo $description; ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <?php if( $column_1 || $column_2 ) { ?>
        <div class="axm-single__columns">
            <div class="container">
                <div class="row">
                    <?php if( $column_1 ) { ?>
                    <div class="col-lg-6">
                        <div class="axm-single__column" data-aos="fade-right" data-aos-duration="500"><?php echo $column_1; ?></div>
                    </div>
                    <?php }
                    if( $column_2 ) { ?>
                    <div class="col-lg-6">
                        <div class="axm-single__column" data-aos="fade-left" data-aos-duration="500"><?php echo $column_2; ?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } 
        $offer_row = get_field('offer_row', 'option');
        $title = $offer_row['title'];
        $button_label = $offer_row['button_label'];
        $button_email = $offer_row['button_email'];
        if( $title || ( $button_label && $button_label ) ) { ?>
        <div class="axm-single__offer">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="axm-single__offer__row" data-aos="fade-up" data-aos-duration="500">
                            <div class="row">
                                <?php if( $title ) { ?>
                                <div class="col-lg-8">
                                    <h5><?php echo $offer_row['title']; ?></h5>
                                </div>
                                <?php } 
                                if( $button_label && $button_email ) { ?>
                                <div class="col-lg-4">
                                    <a class="btn btn__primary full" href="mailto:<?php echo $button_email; ?>?subject=<?php the_title(); ?>"><?php echo $button_label; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </section>
	<?php }
}
get_footer();