<?php
/**
 *
 * @package WordPress
 * @subpackage Axiomo
 * @since 1.0
 * @version 1.0
 */
?>
    </main>
    <footer class="axm-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <?php if( get_field('footer_logo', 'option') ) { ?>
                    <a class="axm-footer__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('footer_logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-lg-3">
                <?php if( get_field('copyrights_text', 'option') ) { ?>
                    <div class="axm-footer__text"><?php the_field('copyrights_text', 'option'); ?></div>
                <?php } ?>
                </div>
                <div class="col-lg-3">
                <?php if( get_field('address', 'option') ) { ?>
                    <div class="axm-footer__text"><?php the_field('address', 'option'); ?></div>
                <?php } ?>
                </div>
                <div class="col-lg-3">
                <?php if( get_field('email', 'option') ) { ?>
                    <div class="axm-footer__text email">
                        <p><?php _e('Email', 'axiomo'); ?></p>
                        <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a> 
                    </div>
                <?php } ?>
                </div>
            </div>
            
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>